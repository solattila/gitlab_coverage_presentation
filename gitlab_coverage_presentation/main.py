import math


def circle_area(radius):
    return math.pi * radius ** 2

def add(a, b):
    return a + b

def is_even(number):
    if number % 2 == 0:
        return True
    else:
        return False
