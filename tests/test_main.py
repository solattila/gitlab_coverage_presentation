import pytest

from gitlab_coverage_presentation.main import circle_area, add, is_even


@pytest.mark.parametrize("radius, expected_area", [
    pytest.param(1, 3.141592653589793, id="radius_1"),
    pytest.param(2, 12.566370614359172, id="radius_2"),
    pytest.param(3, 28.274333882308138, id="radius_3"),
])
def test_circle_area(radius, expected_area):
    # Act
    result = circle_area(radius)

    # Assert
    assert pytest.approx(result, 0.1) == expected_area

@pytest.mark.parametrize("a, b, expected_sum", [
    pytest.param(1, 2, 3, id="1_plus_2"),
    pytest.param(2, 3, 5, id="2_plus_3"),
    pytest.param(3, 4, 7, id="3_plus_4"),
])
def test_add(a, b, expected_sum):
    # Act
    result = add(a, b)

    # Assert
    assert result == expected_sum

@pytest.mark.parametrize("number, expected_result", [
    pytest.param(2, True, id="even_number"),
    pytest.param(4, True, id="another_even_number"),
])
def test_is_even(number, expected_result):
    # Act
    result = is_even(number)

    # Assert
    assert result == expected_result

